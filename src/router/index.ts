import {createRouter, createWebHistory} from 'vue-router';
import DesignSystem from "/src/DesignSystem.vue";
import HomePage from '/src/HomePage.vue';

const routes = [
    {
        path: '/',
        name: 'HomePage',
        component: HomePage
    },
    {
        path: '/design-system',
        name: 'DesignSystem',
        component: DesignSystem
    }
];
const router = createRouter({
    history: createWebHistory(),
    routes
});

export default router;
