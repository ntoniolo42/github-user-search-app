# Frontend Mentor - GitHub user search app solution

This is a solution to the [GitHub user search app challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/github-user-search-app-Q09YOgaH6). Frontend Mentor challenges help you improve your coding skills by building realistic projects. 

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Screenshot](#screenshot)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
- [Author](#author)

**Note: Delete this note and update the table of contents based on what sections you keep.**

## Overview

### The challenge

Users should be able to:

- View the optimal layout for the app depending on their device's screen size
- See hover states for all interactive elements on the page
- Search for GitHub users by their username
- See relevant user information based on their search
- Switch between light and dark themes
- **Bonus**: Have the correct color scheme chosen for them based on their computer preferences. _Hint_: Research `prefers-color-scheme` in CSS.

### Screenshot

![desktop](./screenshots/desktop.png)
![mobile](./screenshots/mobile.png)
![Design System](./screenshots/design-system.png)

### Links

- [Solution](https://gitlab.com/ntoniolo42/github-user-search-app)
- [Live Site](https://frontendmentor.github-user-search.ntoniolo.wtf/)
- [DesignSystem](https://frontendmentor.github-user-search.ntoniolo.wtf/design-system)

## My process

### Built with

- Mobile-first workflow
- [Tailwind](https://tailwindcss.com/)
- [Vue3](https://vuejs.org/)
- [moment](https://momentjs.com/)
- [Vite](https://vitejs.dev/)

### What I learned

- First time used Local Storage
- Switch theme with add/remove dark class of body
- First experience with vue 3 :D

## Author

- Website - [ntoniolo.wtf](https://ntoniolo.wtf)
- Frontend Mentor - [@On-Jin](https://www.frontendmentor.io/profile/On-Jin)
