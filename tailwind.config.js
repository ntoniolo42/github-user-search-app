module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
  ],
  darkMode: 'class',
  theme: {
    fontFamily: {
      mono: ['Space Mono'],
    },
    extend: {
      colors: {
        "clr-primary": "#0079FF",
        "clr-primary-active": "#60abff",
        "clr-blue-content": "#4B6A9B",
        "clr-darken-content": "#2B3442",

        "clr-grey": "#697C9A",

        "clr-dark-darken-bg": "#141D2F",

        "clr-almost-white": "#FEFEFE",

        "clr-bg-light-mode": "#F6F8FF",
        "clr-bg-dark-mode": "#1E2A47",
      },
      // fontSize: {
      //   'fs-h1': "1.625rem",
      //   'fs-h2': "1.375rem",
      //   'fs-h3': "1rem",
      //   'fs-h4': "0.8125rem",
      //   'fs-body': "0.9375rem",
      // },
      // lineHeight: {
      //   'fs-h1': "1.625rem",
      //   'fs-h2': "1.375rem",
      //   'fs-h3': "1rem",
      //   'fs-h4': "0.8125rem",
      //   'fs-body': "0.9375rem",
      // }
    }
  },
  plugins: [],
}
